﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System;
using System.Collections.Generic;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class ShopCartItem : Entity
    {
        public int ShopCartId { get; set; }
        public ShopCart ShopCart { get; set; }

        public int DishId { get; set; }
        public Dish Dish { get; set; }

        public int Quantity { get; set; }

        public DateTime DateAdd { get; set; }

        public bool IsOrdered { get; set; }

        public List<OrderDetail> OrderDetailts { get; set; }
    }
}