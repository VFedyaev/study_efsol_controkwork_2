﻿using System;

namespace FoodDeliveryOrderApp.Core.Models.Base
{
    public class Entity
    {
        public int Id { get; set; }
    }
}