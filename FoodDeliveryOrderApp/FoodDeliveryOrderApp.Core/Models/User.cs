﻿using Microsoft.AspNetCore.Identity;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class User : IdentityUser<int>
    {
    }
}