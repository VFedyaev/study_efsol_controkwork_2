﻿using FoodDeliveryOrderApp.Core.Models.Base;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class OrderDetail : Entity
    {
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public int ShopCartItemId { get; set; }
        public ShopCartItem ShopCartItem { get; set; }

        public double Price { get; set; }
    }
}