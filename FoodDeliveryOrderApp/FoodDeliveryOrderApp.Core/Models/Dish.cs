﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class Dish : Entity
    {
        [Display(Name = "Наименование блюда")]
        public string Name { get; set; }

        [Display(Name = "Цена")]
        public double Price { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        public int InstitutionId { get; set; }
        [Display(Name = "Заведение")]
        public Institution Institution { get; set; }
    }
}