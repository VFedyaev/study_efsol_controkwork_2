﻿using FoodDeliveryOrderApp.Core.Enums;
using FoodDeliveryOrderApp.Core.Models.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class Institution : Entity
    {
        [Display(Name = "Тип заведения")]
        public InstitutionType InstitutionType { get; set; }

        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Заполните поле наименование")]
        public string Name { get; set; }

        [Display(Name = "Фото")]
        public string Image { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        public ICollection<Dish> Dishes { get; set; }
        public ICollection<Review> Reviews { get; set; }

        public Institution()
        {
            Dishes = new List<Dish>();
            Reviews = new List<Review>();
        }
    }
}