﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class ShopCart : Entity
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public DateTime DateCreated { get; set; }
    }
}