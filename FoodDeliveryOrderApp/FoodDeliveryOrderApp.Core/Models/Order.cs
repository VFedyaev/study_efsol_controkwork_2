﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class Order : Entity
    {
        [Display(Name = "Фамилия Имя Отчество")]
        [Required(ErrorMessage = "Заполните поле ФИО")]
        public string FullName { get; set; }

        [Display(Name = "Адрес")]
        [Required(ErrorMessage = "Заполните поле Адрес")]
        public string Address { get; set; }

        [Display(Name = "Телефон")]
        [Required(ErrorMessage = "Заполните поле Телефон")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        public DateTime DateOrder { get; set; }

        public List<OrderDetail> OrderDetailts { get; set; }
    }
}