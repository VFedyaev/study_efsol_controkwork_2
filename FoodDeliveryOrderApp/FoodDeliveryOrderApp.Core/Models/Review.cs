﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.Core.Models
{
    public class Review : Entity
    {
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Display(Name = "Оценка")]
        public int Mark { get; set; }

        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
    }
}