﻿using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.Core.Enums
{
    public enum InstitutionType
    {
        [Display(Name = "Ресторан")]
        Restaurant = 1,
        [Display(Name = "Кафе")]
        Cafe
    }
}