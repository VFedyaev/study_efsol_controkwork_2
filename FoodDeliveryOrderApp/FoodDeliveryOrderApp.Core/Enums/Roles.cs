﻿using System;

namespace FoodDeliveryOrderApp.Core.Enums
{
    public enum Roles
    {
        Admin,
        User
    }
}