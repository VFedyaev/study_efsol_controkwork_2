﻿using FoodDeliveryOrderApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IOrderDetailRepository : IRepository<OrderDetail>
    {
        void CreateOrder(Order order, int shopCartId);
        IEnumerable<OrderDetail> GetInstitutionOrdersById(int id);
    }
}