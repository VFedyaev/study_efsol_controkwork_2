﻿using FoodDeliveryOrderApp.Core.Models;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IReviewRepository : IRepository<Review>
    {
    }
}