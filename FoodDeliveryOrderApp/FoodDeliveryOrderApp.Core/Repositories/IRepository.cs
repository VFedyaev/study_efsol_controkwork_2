﻿using FoodDeliveryOrderApp.Core.Models.Base;
using System.Collections.Generic;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}