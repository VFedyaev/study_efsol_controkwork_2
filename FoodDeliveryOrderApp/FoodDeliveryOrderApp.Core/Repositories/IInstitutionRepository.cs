﻿using FoodDeliveryOrderApp.Core.Models;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IInstitutionRepository : IRepository<Institution>
    {
    }
}