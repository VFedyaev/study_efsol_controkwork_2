﻿using FoodDeliveryOrderApp.Core.Models;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
    }
}