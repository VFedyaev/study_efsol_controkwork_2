﻿using FoodDeliveryOrderApp.Core.Models;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IShopCartRepository : IRepository<ShopCart>
    {
    }
}