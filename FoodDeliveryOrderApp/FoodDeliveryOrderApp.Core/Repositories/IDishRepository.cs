﻿using FoodDeliveryOrderApp.Core.Models;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IDishRepository : IRepository<Dish>
    {
    }
}