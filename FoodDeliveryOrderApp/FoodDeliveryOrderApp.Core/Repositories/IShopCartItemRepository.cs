﻿using FoodDeliveryOrderApp.Core.Models;
using System.Collections.Generic;

namespace FoodDeliveryOrderApp.Core.Repositories
{
    public interface IShopCartItemRepository : IRepository<ShopCartItem>
    {
        void AddToCart(Dish dish, int quantity, int shopCartId);
        void RemoveLine(Dish dish, int shopCartId);
        IEnumerable<ShopCartItem> GetShopCartItems(int shopCartId);
        decimal ComputeTotalValue(int shopCartId);
        void HideOrderedItems(int shopCartId);
    }
}