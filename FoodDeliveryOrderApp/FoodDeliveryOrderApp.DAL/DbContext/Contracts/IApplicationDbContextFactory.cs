﻿using System;

namespace FoodDeliveryOrderApp.DAL.DbContext.Contracts
{
    public interface IApplicationDbContextFactory
    {
        ApplicationDbContext Create();
    }
}