﻿using FoodDeliveryOrderApp.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace FoodDeliveryOrderApp.DAL.DbContext
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<Dish> Dishes { get; set; }

        public DbSet<ShopCart> ShopCarts { get; set; }
        public DbSet<ShopCartItem> ShopCartItems { get; set; }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }

        public DbSet<Review> Reviews { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        { }
    }
}