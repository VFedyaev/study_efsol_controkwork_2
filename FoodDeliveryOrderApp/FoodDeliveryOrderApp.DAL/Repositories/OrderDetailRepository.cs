﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class OrderDetailRepository : Repository<OrderDetail>, IOrderDetailRepository
    {
        public OrderDetailRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.OrderDetails;
        }

        public void CreateOrder(Order order, int shopCartId)
        {
            var shopCartItems = _context.ShopCartItems
             .Where(s => (s.ShopCartId == shopCartId))
             .Where(x => (x.IsOrdered == false)).ToList();

            foreach (var item in shopCartItems)
            {
                DbSet.Add(new OrderDetail()
                {
                    ShopCartItemId = item.Id,
                    OrderId = order.Id,
                    Price = (item.Dish.Price * item.Quantity)
                });

            }
        }

        public IEnumerable<OrderDetail> GetInstitutionOrdersById(int id)
        {
            return DbSet
                .Include(i => i.Order)
                .Include(i => i.ShopCartItem)
                    .ThenInclude(i => i.Dish)
                        .ThenInclude(i => i.Institution)
                .Where(e => e.ShopCartItem.Dish.Institution.Id == id).ToList();
        }
    }
}