﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class InstitutionRepository : Repository<Institution>, IInstitutionRepository
    {
        public InstitutionRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Institutions;
        }

        public override IEnumerable<Institution> GetAll()
        {
            return DbSet.Include(x => x.Reviews).ToList();
        }

        public override Institution GetById(int id)
        {
            return DbSet.Include(i => i.Dishes).FirstOrDefault(e => e.Id == id);
        }
    }
}