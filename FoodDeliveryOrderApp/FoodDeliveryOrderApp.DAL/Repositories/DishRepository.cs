﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Dishes;
        }

        public override IEnumerable<Dish> GetAll()
        {
            return DbSet.Include(i => i.Institution).ToList();
        }

        public override Dish GetById(int id)
        {
            return DbSet.Include(i => i.Institution).FirstOrDefault(e => e.Id == id);
        }
    }
}