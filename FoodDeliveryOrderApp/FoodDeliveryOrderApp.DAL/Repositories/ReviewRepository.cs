﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Reviews;
        }
    }
}