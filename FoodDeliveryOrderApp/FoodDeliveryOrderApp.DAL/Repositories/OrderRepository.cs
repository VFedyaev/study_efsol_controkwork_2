﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using System;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Orders;
        }
    }
}