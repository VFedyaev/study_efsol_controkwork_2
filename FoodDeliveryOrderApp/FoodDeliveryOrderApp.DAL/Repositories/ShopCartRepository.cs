﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class ShopCartRepository : Repository<ShopCart>, IShopCartRepository
    {
        public ShopCartRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.ShopCarts;
        }
    }
}