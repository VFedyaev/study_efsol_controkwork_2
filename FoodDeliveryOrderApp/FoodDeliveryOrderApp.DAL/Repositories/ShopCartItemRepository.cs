﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FoodDeliveryOrderApp.DAL.Repositories
{
    public class ShopCartItemRepository : Repository<ShopCartItem>, IShopCartItemRepository
    {
        public ShopCartItemRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.ShopCartItems;
        }

        //Get all ShopCartItems by ShopCartId and only with mark IsOrdered == false
        public IEnumerable<ShopCartItem> GetShopCartItems(int shopCartId)
        {
            return DbSet
                .Where(c => (c.ShopCartId == shopCartId))
                .Where(x => (x.IsOrdered == false))
                .Include(d => d.Dish).ToList();
        }

        public void AddToCart(Dish dish, int quantity, int shopCartId)
        {
            var shopCartItems = DbSet
                .Where(d => (d.Dish.Id == dish.Id))
                .Where(s => (s.ShopCartId == shopCartId))
                .Where(x => (x.IsOrdered == false)).FirstOrDefault();

            var shopCartIsExist = DbSet.Any(s => s.ShopCartId == shopCartId);

            if (shopCartItems == null || shopCartIsExist == false)
            {
                DbSet.Add(new ShopCartItem
                {
                    ShopCartId = shopCartId,
                    Dish = dish,
                    Quantity = quantity,
                    DateAdd = DateTime.Now,
                    IsOrdered = false
                });
            }
            else if (shopCartIsExist == true)
            {
                shopCartItems.Quantity += quantity;
            }
        }

        public void RemoveLine(Dish dish, int shopCartId)
        {
            var shopCartItems = DbSet
               .Where(d => (d.Dish.Id == dish.Id))
               .Where(s => (s.ShopCartId == shopCartId))
               .Where(x => (x.IsOrdered == false)).FirstOrDefault();

            if (shopCartItems != null)
            {
                DbSet.Remove(shopCartItems);
            }
        }

        public decimal ComputeTotalValue(int shopCartId)
        {
            return Convert.ToDecimal(DbSet.Where(s => (s.ShopCartId == shopCartId)).Where(x => x.IsOrdered == false).Sum(e => e.Dish.Price * e.Quantity));
        }

        //If order is OK set mark IsOrdered == true
        public void HideOrderedItems(int shopCartId)
        {
            var shopCartItems = DbSet.Where(s => s.ShopCartId == shopCartId).ToList();
            if (shopCartItems != null)
            {
                foreach (var item in shopCartItems)
                {
                    item.IsOrdered = true;
                    DbSet.Update(item);
                }
            }
        }
    }
}