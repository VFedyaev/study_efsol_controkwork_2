﻿using FoodDeliveryOrderApp.Core.Repositories;
using System;
using System.Data;
using System.Threading.Tasks;

namespace FoodDeliveryOrderApp.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IInstitutionRepository Institutions { get; }
        IDishRepository Dishes { get; }
        IReviewRepository Reviews { get; }
        IShopCartRepository ShopCarts { get; }
        IShopCartItemRepository ShopCartItems { get; }
        IOrderRepository Orders { get; }
        IOrderDetailRepository OrderDetails { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();
    }
}