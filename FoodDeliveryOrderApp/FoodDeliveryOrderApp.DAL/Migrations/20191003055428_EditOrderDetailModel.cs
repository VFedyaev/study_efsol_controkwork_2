﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class EditOrderDetailModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetailts_Institutions_InstitutionId",
                table: "OrderDetailts");

            migrationBuilder.DropIndex(
                name: "IX_OrderDetailts_InstitutionId",
                table: "OrderDetailts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_OrderDetailts_InstitutionId",
                table: "OrderDetailts",
                column: "InstitutionId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetailts_Institutions_InstitutionId",
                table: "OrderDetailts",
                column: "InstitutionId",
                principalTable: "Institutions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
