﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class updateShopCartModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ShopCarts_UserId",
                table: "ShopCarts",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ShopCarts_AspNetUsers_UserId",
                table: "ShopCarts",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ShopCarts_AspNetUsers_UserId",
                table: "ShopCarts");

            migrationBuilder.DropIndex(
                name: "IX_ShopCarts_UserId",
                table: "ShopCarts");
        }
    }
}
