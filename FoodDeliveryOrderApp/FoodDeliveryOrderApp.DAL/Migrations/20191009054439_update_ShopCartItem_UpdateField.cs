﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class update_ShopCartItem_UpdateField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Dishes_DishId",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_AspNetUsers_UserId",
                table: "OrderDetails");

            migrationBuilder.DropIndex(
                name: "IX_OrderDetails_DishId",
                table: "OrderDetails");

            migrationBuilder.DropColumn(
                name: "DishId",
                table: "OrderDetails");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "OrderDetails",
                newName: "ShopCartItemId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_UserId",
                table: "OrderDetails",
                newName: "IX_OrderDetails_ShopCartItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_ShopCartItems_ShopCartItemId",
                table: "OrderDetails",
                column: "ShopCartItemId",
                principalTable: "ShopCartItems",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_ShopCartItems_ShopCartItemId",
                table: "OrderDetails");

            migrationBuilder.RenameColumn(
                name: "ShopCartItemId",
                table: "OrderDetails",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_ShopCartItemId",
                table: "OrderDetails",
                newName: "IX_OrderDetails_UserId");

            migrationBuilder.AddColumn<int>(
                name: "DishId",
                table: "OrderDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_OrderDetails_DishId",
                table: "OrderDetails",
                column: "DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Dishes_DishId",
                table: "OrderDetails",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_AspNetUsers_UserId",
                table: "OrderDetails",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
