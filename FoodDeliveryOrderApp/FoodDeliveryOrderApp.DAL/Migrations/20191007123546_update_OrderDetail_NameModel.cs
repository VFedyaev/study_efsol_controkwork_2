﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class update_OrderDetail_NameModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetailts_Dishes_DishId",
                table: "OrderDetailts");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetailts_Orders_OrderId",
                table: "OrderDetailts");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetailts_AspNetUsers_UserId",
                table: "OrderDetailts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderDetailts",
                table: "OrderDetailts");

            migrationBuilder.RenameTable(
                name: "OrderDetailts",
                newName: "OrderDetails");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetailts_UserId",
                table: "OrderDetails",
                newName: "IX_OrderDetails_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetailts_OrderId",
                table: "OrderDetails",
                newName: "IX_OrderDetails_OrderId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetailts_DishId",
                table: "OrderDetails",
                newName: "IX_OrderDetails_DishId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderDetails",
                table: "OrderDetails",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Dishes_DishId",
                table: "OrderDetails",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_Orders_OrderId",
                table: "OrderDetails",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetails_AspNetUsers_UserId",
                table: "OrderDetails",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Dishes_DishId",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_Orders_OrderId",
                table: "OrderDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderDetails_AspNetUsers_UserId",
                table: "OrderDetails");

            migrationBuilder.DropPrimaryKey(
                name: "PK_OrderDetails",
                table: "OrderDetails");

            migrationBuilder.RenameTable(
                name: "OrderDetails",
                newName: "OrderDetailts");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_UserId",
                table: "OrderDetailts",
                newName: "IX_OrderDetailts_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_OrderId",
                table: "OrderDetailts",
                newName: "IX_OrderDetailts_OrderId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderDetails_DishId",
                table: "OrderDetailts",
                newName: "IX_OrderDetailts_DishId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_OrderDetailts",
                table: "OrderDetailts",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetailts_Dishes_DishId",
                table: "OrderDetailts",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetailts_Orders_OrderId",
                table: "OrderDetailts",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderDetailts_AspNetUsers_UserId",
                table: "OrderDetailts",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
