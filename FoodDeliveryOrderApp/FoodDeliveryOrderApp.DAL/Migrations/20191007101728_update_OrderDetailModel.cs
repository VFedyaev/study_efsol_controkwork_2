﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class update_OrderDetailModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InstitutionId",
                table: "OrderDetailts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "InstitutionId",
                table: "OrderDetailts",
                nullable: false,
                defaultValue: 0);
        }
    }
}
