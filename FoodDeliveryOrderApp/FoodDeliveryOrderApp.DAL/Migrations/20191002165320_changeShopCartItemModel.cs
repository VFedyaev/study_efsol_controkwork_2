﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FoodDeliveryOrderApp.DAL.Migrations
{
    public partial class changeShopCartItemModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "ShopCartItems");

            migrationBuilder.RenameColumn(
                name: "ItemId",
                table: "ShopCartItems",
                newName: "Quantity");

            migrationBuilder.AddColumn<string>(
                name: "ShopCartId",
                table: "ShopCartItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ShopCartId",
                table: "ShopCartItems");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "ShopCartItems",
                newName: "ItemId");

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "ShopCartItems",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
