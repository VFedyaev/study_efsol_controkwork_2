﻿using System;

namespace FoodDeliveryOrderApp.DAL
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}