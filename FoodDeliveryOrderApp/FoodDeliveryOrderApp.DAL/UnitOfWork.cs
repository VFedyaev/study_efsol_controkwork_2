﻿using System;
using System.Data;
using System.Threading.Tasks;
using FoodDeliveryOrderApp.Core.Repositories;
using FoodDeliveryOrderApp.DAL.DbContext;
using FoodDeliveryOrderApp.DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace FoodDeliveryOrderApp.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private IDbContextTransaction _transaction;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Institutions = new InstitutionRepository(_context);
            Dishes = new DishRepository(_context);
            Reviews = new ReviewRepository(_context);
            ShopCarts = new ShopCartRepository(_context);
            ShopCartItems = new ShopCartItemRepository(_context);
            Orders = new OrderRepository(_context);
            OrderDetails = new OrderDetailRepository(_context);
        }

        public IInstitutionRepository Institutions { get; }
        public IDishRepository Dishes { get; }
        public IReviewRepository Reviews { get; }
        public IShopCartRepository ShopCarts { get; }
        public IShopCartItemRepository ShopCartItems { get; }
        public IOrderRepository Orders { get; }
        public IOrderDetailRepository OrderDetails { get; }

        public Task<int> CompleteAsync()
        {
            return _context.SaveChangesAsync();
        }
        public void BeginTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        public void BeginTransaction(IsolationLevel level)
        {
            _transaction = _context.Database.BeginTransaction(level);
        }

        public void RollbackTransaction()
        {
            if (_transaction == null) return;

            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;

        }

        public void CommitTransaction()
        {
            if (_transaction == null) return;

            _transaction.Commit();
            _transaction.Dispose();
            _transaction = null;
        }

        private bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
                this._disposed = true;
            }
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}