﻿using AutoMapper;
using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.WEB.ViewModels;

namespace FoodDeliveryOrderApp.WEB
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            DishMapping();
        }

        private void DishMapping()
        {
            CreateMap<Dish, DishCreateViewModel>().ReverseMap();
            CreateMap<Dish, DishEditViewModel>().ReverseMap();
        }
    }
}
