﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.DAL;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using FoodDeliveryOrderApp.WEB.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;

namespace FoodDeliveryOrderApp.WEB.Controllers
{
    public class InstitutionController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public InstitutionController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: Institution
        [AllowAnonymous]
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                return View(unitOfWork.Institutions.GetAll());
            }
        }

        // GET: Institution/Details/5
        [Authorize(Roles = "Admin, User")]
        public IActionResult Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var institution = unitOfWork.Institutions.GetById(id.Value);
                if (institution == null)
                {
                    return NotFound();
                }

                return View(institution);
            }
        }

        // GET: Institution/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Institution/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InstitutionType,Name,Image,Description,Id")] Institution institution, IFormFile file)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    if (file == null)
                    {
                        institution.Image = "default-image.jpg";
                    }
                    else
                    {
                        UploadFile(file, institution);
                    }

                    unitOfWork.Institutions.Add(institution);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }
                return View(institution);
            }
        }

        // GET: Institution/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var institution = unitOfWork.Institutions.GetById(id.Value);
                if (institution == null)
                {
                    return NotFound();
                }
                return View(institution);
            }
        }

        // POST: Institution/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("InstitutionType,Name,Image,Description,Id")] Institution institution, IFormFile file)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (id != institution.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        if (file == null)
                        {
                            institution.Image = "default-image.jpg";
                        }
                        else
                        {
                            UploadFile(file, institution);
                        }
                        unitOfWork.Institutions.Update(institution);
                        await unitOfWork.CompleteAsync();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        throw ex;
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(institution);
            }
        }

        // GET: Institution/Delete/5
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var institution = unitOfWork.Institutions.GetById(id.Value);
                if (institution == null)
                {
                    return NotFound();
                }

                return View(institution);
            }
        }

        // POST: Institution/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var institution = unitOfWork.Institutions.GetById(id);
                unitOfWork.Institutions.Delete(institution);
                await unitOfWork.CompleteAsync();

                return RedirectToAction(nameof(Index));
            }
        }

        [Authorize(Roles = "Admin")]
        public void UploadFile(IFormFile file, Institution institution)
        {
            var fileName = file.FileName;
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", fileName);
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                file.CopyTo(fileStream);
            }

            institution.Image = fileName;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult InstitutionOrders(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var institutions = unitOfWork.OrderDetails.GetInstitutionOrdersById(id.Value);

                if (!institutions.Any())
                {
                    return RedirectToAction("EmptyInstitutionOrders");
                }

                var viewModels = institutions.Select(institution => new InstituionOrdersViewModel
                {
                    InstitutionName = institution.ShopCartItem.Dish.Institution.Name,
                    FullName = institution.Order.FullName,
                    DateOrder = institution.Order.DateOrder,
                    DishName = institution.ShopCartItem.Dish.Name,
                    Quantity = institution.ShopCartItem.Quantity,
                    Price = institution.ShopCartItem.Dish.Price,
                    Sum = institution.Price
                });

                return View(viewModels);
            }
        }

        [Authorize(Roles = "Admin, User")]
        public IActionResult EmptyInstitutionOrders()
        {
            return View();
        }
    }
}