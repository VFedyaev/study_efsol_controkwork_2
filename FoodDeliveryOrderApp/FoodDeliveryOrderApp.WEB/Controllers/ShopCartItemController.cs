﻿using System;
using System.Linq;
using System.Threading.Tasks;
using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FoodDeliveryOrderApp.WEB.Controllers
{
    public class ShopCartItemController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public ShopCartItemController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        [Authorize(Roles = "User")]
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var userId = _userManager.GetUserId(User);
                var shopCartId = unitOfWork.ShopCarts.GetAll()
                    .Where(x => x.UserId == Convert.ToInt32(userId))
                    .Select(com => com.Id).FirstOrDefault();

                ViewBag.ComputeTotalValue = unitOfWork.ShopCartItems.ComputeTotalValue(shopCartId);

                return View(unitOfWork.ShopCartItems.GetShopCartItems(shopCartId));
            }
        }

        [Authorize(Roles = "User")]
        public async Task<RedirectToActionResult> AddToCart(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var dish = unitOfWork.Dishes.GetById(id);
                var userId = _userManager.GetUserId(User);
                var shopCartId = unitOfWork.ShopCarts.GetAll().Where(x => x.UserId == Convert.ToInt32(userId)).Select(com => com.Id).FirstOrDefault();

                if (dish != null)
                {
                    unitOfWork.ShopCartItems.AddToCart(dish, 1, shopCartId);
                    await unitOfWork.CompleteAsync();
                }
                return RedirectToAction("Index");
            }
        }

        [Authorize(Roles = "User")]
        public async Task<RedirectToActionResult> RemoveFromCart(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var dish = unitOfWork.Dishes.GetById(id);
                var userId = _userManager.GetUserId(User);
                var shopCartId = unitOfWork.ShopCarts.GetAll().Where(x => x.UserId == Convert.ToInt32(userId)).Select(com => com.Id).FirstOrDefault();

                if (dish != null)
                {
                    unitOfWork.ShopCartItems.RemoveLine(dish, shopCartId);
                    await unitOfWork.CompleteAsync();
                }

                return RedirectToAction("Index");
            }
        }
    }
}