﻿using System.Threading.Tasks;
using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FoodDeliveryOrderApp.WEB.Controllers
{
    public class ReviewController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public ReviewController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        [Authorize(Roles = "User")]
        public IActionResult Create(int institutionId)
        {
            ViewBag.InstitutionId = institutionId;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Review review)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    unitOfWork.Reviews.Add(review);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction("Index", "Institution");
                }
                return View(review);
            }
        }
    }
}