﻿using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDeliveryOrderApp.WEB.Controllers
{
    public class OrderController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly UserManager<User> _userManager;

        public OrderController(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        [Authorize(Roles = "User")]
        public IActionResult Checkout()
        {
            return View();
        }

        [Authorize(Roles = "User")]
        [HttpPost]
        public async Task<IActionResult> Checkout(Order order)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var userId = _userManager.GetUserId(User);
                var shopCartId = unitOfWork.ShopCarts.GetAll()
                    .Where(x => x.UserId == Convert.ToInt32(userId))
                    .Select(com => com.Id).FirstOrDefault();

                var shopCartItems = unitOfWork.ShopCartItems.GetShopCartItems(shopCartId);

                if (shopCartItems.Count() == 0)
                {
                    ModelState.AddModelError("", "У вас нет ни одного товара в корзине!");
                }

                if (ModelState.IsValid)
                {
                    order = (new Order
                    {
                        FullName = order.FullName,
                        Address = order.Address,
                        Phone = order.Phone,
                        Email = order.Email,
                        DateOrder = DateTime.Now
                    });

                    unitOfWork.Orders.Add(order);
                    await unitOfWork.CompleteAsync();

                    unitOfWork.OrderDetails.CreateOrder(order, shopCartId);
                    unitOfWork.ShopCartItems.HideOrderedItems(shopCartId);
                    await unitOfWork.CompleteAsync();
                    return RedirectToAction("Complete");
                }
            }
            return View(order);
        }

        [Authorize(Roles = "User")]
        public IActionResult Complete()
        {
            return View();
        }
    }
}