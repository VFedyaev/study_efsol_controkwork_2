﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FoodDeliveryOrderApp.Core.Models;
using FoodDeliveryOrderApp.DAL;
using FoodDeliveryOrderApp.WEB.ViewModels;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace FoodDeliveryOrderApp.WEB.Controllers
{
    public class DishController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DishController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        // GET: Dish
        [AllowAnonymous]
        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                return View(unitOfWork.Dishes.GetAll());
            }
        }

        // GET: Dish/Details/5
        [Authorize(Roles = "Admin, User")]
        public IActionResult Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var dish = unitOfWork.Dishes.GetById(id.Value);
                if (dish == null)
                {
                    return NotFound();
                }

                return View(dish);
            }
        }

        // GET: Dish/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var model = new DishCreateViewModel
                {
                    InstitutionSelectList = new SelectList(unitOfWork.Institutions.GetAll(), "Id", "Name")
                };
                return View(model);
            }
        }

        // POST: Dish/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Price,Description,InstitutionId,Id")] DishCreateViewModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    var dish = Mapper.Map<Dish>(model);
                    unitOfWork.Dishes.Add(dish);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }
                model.InstitutionSelectList = new SelectList(unitOfWork.Institutions.GetAll(), "Id", "Name", model.InstitutionId);
                return View(model);
            }
        }

        // GET: Dish/Edit/5
        [Authorize(Roles = "Admin")]
        public IActionResult Edit(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var dish = unitOfWork.Dishes.GetById(id.Value);

                var model = Mapper.Map<DishEditViewModel>(dish);
                model.InstitutionSelectList = new SelectList(unitOfWork.Institutions.GetAll(), "Id", "Name", model.InstitutionId);

                if (dish == null)
                {
                    return NotFound();
                }

                return View(model);
            }
        }

        // POST: Dish/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Name,Price,Description,InstitutionId,Id")] DishEditViewModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    var dish = Mapper.Map<Dish>(model);

                    unitOfWork.Dishes.Update(dish);
                    await unitOfWork.CompleteAsync();

                    return RedirectToAction(nameof(Index));
                }
                model.InstitutionSelectList = new SelectList(unitOfWork.Institutions.GetAll(), "Id", "Name", model.InstitutionId);
                return View(model);
            }
        }

        // GET: Dish/Delete/5
        [Authorize(Roles = "Admin")]
        public IActionResult Delete(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var dish = unitOfWork.Dishes.GetById(id.Value);
                if (dish == null)
                {
                    return NotFound();
                }

                return View(dish);
            }
        }

        // POST: Dish/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var dish = unitOfWork.Dishes.GetById(id);
                unitOfWork.Dishes.Delete(dish);
                await unitOfWork.CompleteAsync();

                return RedirectToAction(nameof(Index));
            }
        }
    }
}
