﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FoodDeliveryOrderApp.WEB.ViewModels
{
    public class InstituionOrdersViewModel
    {
        [Display(Name = "Наименование заведения")]
        public string InstitutionName { get; set; }

        [Display(Name = "Фамилия Имя Отчество")]
        public string FullName { get; set; }

        [Display(Name = "Дата заказа")]
        public DateTime DateOrder { get; set; }

        [Display(Name = "Наименование блюда")]
        public string DishName { get; set; }

        [Display(Name = "Количество")]
        public int Quantity { get; set; }

        [Display(Name = "Цена")]
        public double Price { get; set; }

        [Display(Name = "Сумма")]
        public double Sum { get; set; }
    }
}