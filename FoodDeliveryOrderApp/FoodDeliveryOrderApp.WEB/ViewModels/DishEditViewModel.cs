﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace FoodDeliveryOrderApp.WEB.ViewModels
{
    public class DishEditViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required(ErrorMessage = "Заполните поле наименование блюда!")]
        [Display(Name = "Наименование блюда")]
        public string Name { get; set; }

        [Display(Name = "Цена")]
        public double Price { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Необходимо выбрать заведение!")]
        [Display(Name = "Заведение")]
        public int InstitutionId { get; set; }

        public SelectList InstitutionSelectList { get; set; }
    }
}
